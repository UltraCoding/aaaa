﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuarios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnModificaciones = New System.Windows.Forms.Button()
        Me.btnBajas = New System.Windows.Forms.Button()
        Me.btnAltas = New System.Windows.Forms.Button()
        Me.btnConsultas = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnModificaciones
        '
        Me.btnModificaciones.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificaciones.Location = New System.Drawing.Point(339, 209)
        Me.btnModificaciones.Name = "btnModificaciones"
        Me.btnModificaciones.Size = New System.Drawing.Size(231, 66)
        Me.btnModificaciones.TabIndex = 13
        Me.btnModificaciones.Text = "Modificaciones"
        Me.btnModificaciones.UseVisualStyleBackColor = True
        '
        'btnBajas
        '
        Me.btnBajas.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBajas.Location = New System.Drawing.Point(46, 209)
        Me.btnBajas.Name = "btnBajas"
        Me.btnBajas.Size = New System.Drawing.Size(231, 66)
        Me.btnBajas.TabIndex = 12
        Me.btnBajas.Text = "Bajas"
        Me.btnBajas.UseVisualStyleBackColor = True
        '
        'btnAltas
        '
        Me.btnAltas.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAltas.Location = New System.Drawing.Point(339, 95)
        Me.btnAltas.Name = "btnAltas"
        Me.btnAltas.Size = New System.Drawing.Size(231, 66)
        Me.btnAltas.TabIndex = 11
        Me.btnAltas.Text = "Altas"
        Me.btnAltas.UseVisualStyleBackColor = True
        '
        'btnConsultas
        '
        Me.btnConsultas.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultas.Location = New System.Drawing.Point(46, 95)
        Me.btnConsultas.Name = "btnConsultas"
        Me.btnConsultas.Size = New System.Drawing.Size(231, 66)
        Me.btnConsultas.TabIndex = 10
        Me.btnConsultas.Text = "Consultas"
        Me.btnConsultas.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(30, 316)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 24
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'frmUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnModificaciones)
        Me.Controls.Add(Me.btnBajas)
        Me.Controls.Add(Me.btnAltas)
        Me.Controls.Add(Me.btnConsultas)
        Me.Name = "frmUsuarios"
        Me.Text = "Usuarios"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnModificaciones As Button
    Friend WithEvents btnBajas As Button
    Friend WithEvents btnAltas As Button
    Friend WithEvents btnConsultas As Button
    Friend WithEvents btnVolver As Button
End Class
