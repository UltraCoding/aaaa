﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegistroDeProceso
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbNombreDelProceso = New System.Windows.Forms.ComboBox()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.lblFechaFin = New System.Windows.Forms.Label()
        Me.lblFechaInicio = New System.Windows.Forms.Label()
        Me.lblNombreDelProceso = New System.Windows.Forms.Label()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.cbIdRecipiente = New System.Windows.Forms.ComboBox()
        Me.dtpFechaDeInicio = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaDeFin = New System.Windows.Forms.DateTimePicker()
        Me.SuspendLayout()
        '
        'cbNombreDelProceso
        '
        Me.cbNombreDelProceso.FormattingEnabled = True
        Me.cbNombreDelProceso.Location = New System.Drawing.Point(264, 59)
        Me.cbNombreDelProceso.Name = "cbNombreDelProceso"
        Me.cbNombreDelProceso.Size = New System.Drawing.Size(150, 21)
        Me.cbNombreDelProceso.TabIndex = 20
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(79, 296)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 17
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(462, 296)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 16
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'lblFechaFin
        '
        Me.lblFechaFin.AutoSize = True
        Me.lblFechaFin.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFin.Location = New System.Drawing.Point(94, 184)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.Size = New System.Drawing.Size(83, 19)
        Me.lblFechaFin.TabIndex = 15
        Me.lblFechaFin.Text = "Fecha de fin"
        '
        'lblFechaInicio
        '
        Me.lblFechaInicio.AutoSize = True
        Me.lblFechaInicio.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaInicio.Location = New System.Drawing.Point(94, 139)
        Me.lblFechaInicio.Name = "lblFechaInicio"
        Me.lblFechaInicio.Size = New System.Drawing.Size(100, 19)
        Me.lblFechaInicio.TabIndex = 14
        Me.lblFechaInicio.Text = "Fecha de inicio"
        '
        'lblNombreDelProceso
        '
        Me.lblNombreDelProceso.AutoSize = True
        Me.lblNombreDelProceso.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombreDelProceso.Location = New System.Drawing.Point(94, 61)
        Me.lblNombreDelProceso.Name = "lblNombreDelProceso"
        Me.lblNombreDelProceso.Size = New System.Drawing.Size(135, 19)
        Me.lblNombreDelProceso.TabIndex = 13
        Me.lblNombreDelProceso.Text = "Nombre del proceso"
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(95, 99)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(84, 19)
        Me.lblIdRecipiente.TabIndex = 21
        Me.lblIdRecipiente.Text = "Id recipiente"
        '
        'cbIdRecipiente
        '
        Me.cbIdRecipiente.FormattingEnabled = True
        Me.cbIdRecipiente.Location = New System.Drawing.Point(264, 100)
        Me.cbIdRecipiente.Name = "cbIdRecipiente"
        Me.cbIdRecipiente.Size = New System.Drawing.Size(42, 21)
        Me.cbIdRecipiente.TabIndex = 22
        '
        'dtpFechaDeInicio
        '
        Me.dtpFechaDeInicio.Location = New System.Drawing.Point(264, 139)
        Me.dtpFechaDeInicio.Name = "dtpFechaDeInicio"
        Me.dtpFechaDeInicio.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaDeInicio.TabIndex = 23
        '
        'dtpFechaDeFin
        '
        Me.dtpFechaDeFin.Location = New System.Drawing.Point(264, 184)
        Me.dtpFechaDeFin.Name = "dtpFechaDeFin"
        Me.dtpFechaDeFin.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaDeFin.TabIndex = 24
        '
        'frmRegistroDeProceso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.dtpFechaDeFin)
        Me.Controls.Add(Me.dtpFechaDeInicio)
        Me.Controls.Add(Me.cbIdRecipiente)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Controls.Add(Me.cbNombreDelProceso)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.lblFechaFin)
        Me.Controls.Add(Me.lblFechaInicio)
        Me.Controls.Add(Me.lblNombreDelProceso)
        Me.Name = "frmRegistroDeProceso"
        Me.Text = "Registro de proceso"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbNombreDelProceso As System.Windows.Forms.ComboBox
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents btnIngresar As System.Windows.Forms.Button
    Friend WithEvents lblFechaFin As System.Windows.Forms.Label
    Friend WithEvents lblFechaInicio As System.Windows.Forms.Label
    Friend WithEvents lblNombreDelProceso As System.Windows.Forms.Label
    Friend WithEvents lblIdRecipiente As System.Windows.Forms.Label
    Friend WithEvents cbIdRecipiente As System.Windows.Forms.ComboBox
    Friend WithEvents dtpFechaDeInicio As DateTimePicker
    Friend WithEvents dtpFechaDeFin As DateTimePicker
End Class
