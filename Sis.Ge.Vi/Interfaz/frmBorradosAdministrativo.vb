﻿Public Class frmBorradosAdministrativo
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmMenuAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnProducto_Click(sender As Object, e As EventArgs) Handles btnProducto.Click
        frmBorradoProducto.Show()
        Dispose()
    End Sub

    Private Sub btnRecipiente_Click(sender As Object, e As EventArgs) Handles btnRecipiente.Click
        frmBorradoRecipiente.Show()
        Dispose()
    End Sub

    Private Sub btnUva_Click(sender As Object, e As EventArgs) Handles btnUva.Click
        frmBorradoUva.Show()
        Dispose()
    End Sub

    Private Sub btnProceso_Click(sender As Object, e As EventArgs) Handles btnProceso.Click
        frmBorradoProceso.Show()
        Dispose()
    End Sub
End Class