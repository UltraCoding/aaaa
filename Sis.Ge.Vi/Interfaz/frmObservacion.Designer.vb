﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmObservacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.lblObservaciones = New System.Windows.Forms.Label()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.cbIdRecipiente = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Location = New System.Drawing.Point(219, 131)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(344, 97)
        Me.txtObservaciones.TabIndex = 13
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(86, 289)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 12
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(447, 289)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 11
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObservaciones.Location = New System.Drawing.Point(62, 130)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(99, 19)
        Me.lblObservaciones.TabIndex = 8
        Me.lblObservaciones.Text = "Observaciones"
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(62, 72)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(103, 19)
        Me.lblIdRecipiente.TabIndex = 14
        Me.lblIdRecipiente.Text = "Id de recipiente"
        '
        'cbIdRecipiente
        '
        Me.cbIdRecipiente.FormattingEnabled = True
        Me.cbIdRecipiente.Location = New System.Drawing.Point(219, 72)
        Me.cbIdRecipiente.Name = "cbIdRecipiente"
        Me.cbIdRecipiente.Size = New System.Drawing.Size(50, 21)
        Me.cbIdRecipiente.TabIndex = 16
        '
        'frmObservacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.cbIdRecipiente)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.lblObservaciones)
        Me.Name = "frmObservacion"
        Me.Text = "Observación"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents btnIngresar As System.Windows.Forms.Button
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents lblIdRecipiente As System.Windows.Forms.Label
    Friend WithEvents cbIdRecipiente As System.Windows.Forms.ComboBox
End Class
