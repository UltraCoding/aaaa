﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificacionRecipiente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtCapacidad = New System.Windows.Forms.TextBox()
        Me.lblCapacidad = New System.Windows.Forms.Label()
        Me.cbTipo = New System.Windows.Forms.ComboBox()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.lblProceso = New System.Windows.Forms.Label()
        Me.txtIdRecipiente = New System.Windows.Forms.TextBox()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.btnIngresar1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtCapacidad
        '
        Me.txtCapacidad.Location = New System.Drawing.Point(334, 200)
        Me.txtCapacidad.Name = "txtCapacidad"
        Me.txtCapacidad.Size = New System.Drawing.Size(100, 20)
        Me.txtCapacidad.TabIndex = 28
        '
        'lblCapacidad
        '
        Me.lblCapacidad.AutoSize = True
        Me.lblCapacidad.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCapacidad.Location = New System.Drawing.Point(145, 199)
        Me.lblCapacidad.Name = "lblCapacidad"
        Me.lblCapacidad.Size = New System.Drawing.Size(136, 19)
        Me.lblCapacidad.TabIndex = 27
        Me.lblCapacidad.Text = "Capacidad (en litros)"
        '
        'cbTipo
        '
        Me.cbTipo.FormattingEnabled = True
        Me.cbTipo.Location = New System.Drawing.Point(334, 161)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Size = New System.Drawing.Size(151, 21)
        Me.cbTipo.TabIndex = 26
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(96, 294)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 25
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(446, 294)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 24
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'lblProceso
        '
        Me.lblProceso.AutoSize = True
        Me.lblProceso.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProceso.Location = New System.Drawing.Point(145, 164)
        Me.lblProceso.Name = "lblProceso"
        Me.lblProceso.Size = New System.Drawing.Size(36, 19)
        Me.lblProceso.TabIndex = 23
        Me.lblProceso.Text = "Tipo"
        '
        'txtIdRecipiente
        '
        Me.txtIdRecipiente.Location = New System.Drawing.Point(334, 88)
        Me.txtIdRecipiente.Name = "txtIdRecipiente"
        Me.txtIdRecipiente.Size = New System.Drawing.Size(100, 20)
        Me.txtIdRecipiente.TabIndex = 30
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(145, 87)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(106, 19)
        Me.lblIdRecipiente.TabIndex = 29
        Me.lblIdRecipiente.Text = "Id del recipiente"
        '
        'btnIngresar1
        '
        Me.btnIngresar1.Location = New System.Drawing.Point(446, 85)
        Me.btnIngresar1.Name = "btnIngresar1"
        Me.btnIngresar1.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar1.TabIndex = 31
        Me.btnIngresar1.Text = "Ingresar"
        Me.btnIngresar1.UseVisualStyleBackColor = True
        '
        'frmModificacionRecipiente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnIngresar1)
        Me.Controls.Add(Me.txtIdRecipiente)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Controls.Add(Me.txtCapacidad)
        Me.Controls.Add(Me.lblCapacidad)
        Me.Controls.Add(Me.cbTipo)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.lblProceso)
        Me.Name = "frmModificacionRecipiente"
        Me.Text = "Modificaciones de Recipientes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtCapacidad As TextBox
    Friend WithEvents lblCapacidad As Label
    Friend WithEvents cbTipo As ComboBox
    Friend WithEvents btnVolver As Button
    Friend WithEvents btnIngresar As Button
    Friend WithEvents lblProceso As Label
    Friend WithEvents txtIdRecipiente As TextBox
    Friend WithEvents lblIdRecipiente As Label
    Friend WithEvents btnIngresar1 As Button
End Class
