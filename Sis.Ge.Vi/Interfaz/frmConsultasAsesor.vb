﻿Public Class frmConsultasAsesor
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmMenuAsesor.Show()
        Dispose()
    End Sub

    Private Sub btnUvas_Click(sender As Object, e As EventArgs) Handles btnUvas.Click
        frmConsultaDeUvas.Show()
        Dispose()
    End Sub

    Private Sub btnProcesos_Click(sender As Object, e As EventArgs) Handles btnProcesos.Click
        frmConsultaProceso.Show()
        Dispose()
    End Sub

    Private Sub btnRecipientes_Click(sender As Object, e As EventArgs) Handles btnRecipientes.Click
        frmConsultaRecipiente.Show()
        Dispose()
    End Sub

    Private Sub btnProductos_Click(sender As Object, e As EventArgs) Handles btnProductos.Click
        frmConsultaProducto.Show()
        Dispose()
    End Sub
End Class