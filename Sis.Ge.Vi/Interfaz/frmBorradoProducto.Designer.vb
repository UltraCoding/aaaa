﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBorradoProducto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbTipoDeRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblTipoDeRecipiente = New System.Windows.Forms.Label()
        Me.cbIdRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.btnBorrar = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbTipoDeRecipiente
        '
        Me.cbTipoDeRecipiente.FormattingEnabled = True
        Me.cbTipoDeRecipiente.Location = New System.Drawing.Point(307, 84)
        Me.cbTipoDeRecipiente.Name = "cbTipoDeRecipiente"
        Me.cbTipoDeRecipiente.Size = New System.Drawing.Size(121, 21)
        Me.cbTipoDeRecipiente.TabIndex = 26
        '
        'lblTipoDeRecipiente
        '
        Me.lblTipoDeRecipiente.AutoSize = True
        Me.lblTipoDeRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeRecipiente.Location = New System.Drawing.Point(167, 83)
        Me.lblTipoDeRecipiente.Name = "lblTipoDeRecipiente"
        Me.lblTipoDeRecipiente.Size = New System.Drawing.Size(117, 19)
        Me.lblTipoDeRecipiente.TabIndex = 25
        Me.lblTipoDeRecipiente.Text = "Tipo de recipiente"
        '
        'cbIdRecipiente
        '
        Me.cbIdRecipiente.FormattingEnabled = True
        Me.cbIdRecipiente.Location = New System.Drawing.Point(307, 119)
        Me.cbIdRecipiente.Name = "cbIdRecipiente"
        Me.cbIdRecipiente.Size = New System.Drawing.Size(121, 21)
        Me.cbIdRecipiente.TabIndex = 23
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(167, 118)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(84, 19)
        Me.lblIdRecipiente.TabIndex = 21
        Me.lblIdRecipiente.Text = "Id recipiente"
        '
        'btnBorrar
        '
        Me.btnBorrar.Location = New System.Drawing.Point(467, 119)
        Me.btnBorrar.Name = "btnBorrar"
        Me.btnBorrar.Size = New System.Drawing.Size(75, 23)
        Me.btnBorrar.TabIndex = 27
        Me.btnBorrar.Text = "Borrar"
        Me.btnBorrar.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVolver.Location = New System.Drawing.Point(256, 194)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(97, 45)
        Me.btnVolver.TabIndex = 28
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'frmBorradoProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnBorrar)
        Me.Controls.Add(Me.cbTipoDeRecipiente)
        Me.Controls.Add(Me.lblTipoDeRecipiente)
        Me.Controls.Add(Me.cbIdRecipiente)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Name = "frmBorradoProducto"
        Me.Text = "Borrado de productos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbTipoDeRecipiente As ComboBox
    Friend WithEvents lblTipoDeRecipiente As Label
    Friend WithEvents cbIdRecipiente As ComboBox
    Friend WithEvents lblIdRecipiente As Label
    Friend WithEvents btnBorrar As Button
    Friend WithEvents btnVolver As Button
End Class
