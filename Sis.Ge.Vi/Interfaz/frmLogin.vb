﻿Imports Dominio
Imports BL

Public Class frmLogin
    Private user As New Usuario
    Function getRole() As Integer
        Return user.rol
    End Function
    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click

        Dim logic As New Class1
        user.ci = txtCi.Text
        user.password = txtContraseña.Text
        user = logic.login(user)
        Redirect(user)
    End Sub

    Private Sub Redirect(user As Usuario)
        Select Case user.rol
            Case Usuario.EnumRol.Gerente
                frmMenuGerente.Show()
                Hide()
            Case Usuario.EnumRol.Administrativo
                frmMenuAdministrativo.Show()
                Hide()
            Case Usuario.EnumRol.Cliente
                frmMenuCliente.Show()
                Hide()
            Case Usuario.EnumRol.Asesor
                frmMenuAsesor.Show()
                Hide()
            Case Usuario.EnumRol.Conductor
                frmMenuConductor.Show()
                Hide()
        End Select
    End Sub
End Class