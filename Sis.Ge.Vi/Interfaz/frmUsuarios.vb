﻿Public Class frmUsuarios
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmMenuGerente.Show()
        Dispose()
    End Sub

    Private Sub btnConsultas_Click(sender As Object, e As EventArgs) Handles btnConsultas.Click
        frmConsultaUsuario.Show()
        Dispose()
    End Sub

    Private Sub btnAltas_Click(sender As Object, e As EventArgs) Handles btnAltas.Click
        frmAltasUsuarios.Show()
        Dispose()
    End Sub

    Private Sub btnBajas_Click(sender As Object, e As EventArgs) Handles btnBajas.Click
        frmBajaUsuario.Show()
        Dispose()
    End Sub

    Private Sub btnModificaciones_Click(sender As Object, e As EventArgs) Handles btnModificaciones.Click
        frmModificacionUsuario.Show()
        Dispose()
    End Sub
End Class
