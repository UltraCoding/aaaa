﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngresos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnRecipiente = New System.Windows.Forms.Button()
        Me.btnUva = New System.Windows.Forms.Button()
        Me.btnProceso = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnTrasiego = New System.Windows.Forms.Button()
        Me.btnProducto = New System.Windows.Forms.Button()
        Me.btnProceso1 = New System.Windows.Forms.Button()
        Me.lblCi = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnRecipiente
        '
        Me.btnRecipiente.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecipiente.Location = New System.Drawing.Point(415, 68)
        Me.btnRecipiente.Name = "btnRecipiente"
        Me.btnRecipiente.Size = New System.Drawing.Size(159, 66)
        Me.btnRecipiente.TabIndex = 3
        Me.btnRecipiente.Text = "Recipiente"
        Me.btnRecipiente.UseVisualStyleBackColor = True
        '
        'btnUva
        '
        Me.btnUva.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUva.Location = New System.Drawing.Point(50, 68)
        Me.btnUva.Name = "btnUva"
        Me.btnUva.Size = New System.Drawing.Size(159, 66)
        Me.btnUva.TabIndex = 4
        Me.btnUva.Text = "Uva"
        Me.btnUva.UseVisualStyleBackColor = True
        '
        'btnProceso
        '
        Me.btnProceso.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProceso.Location = New System.Drawing.Point(232, 68)
        Me.btnProceso.Name = "btnProceso"
        Me.btnProceso.Size = New System.Drawing.Size(159, 66)
        Me.btnProceso.TabIndex = 5
        Me.btnProceso.Text = "Proceso"
        Me.btnProceso.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(24, 321)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 24
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnTrasiego
        '
        Me.btnTrasiego.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTrasiego.Location = New System.Drawing.Point(232, 211)
        Me.btnTrasiego.Name = "btnTrasiego"
        Me.btnTrasiego.Size = New System.Drawing.Size(159, 66)
        Me.btnTrasiego.TabIndex = 26
        Me.btnTrasiego.Text = "Trasiego"
        Me.btnTrasiego.UseVisualStyleBackColor = True
        '
        'btnProducto
        '
        Me.btnProducto.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProducto.Location = New System.Drawing.Point(50, 211)
        Me.btnProducto.Name = "btnProducto"
        Me.btnProducto.Size = New System.Drawing.Size(159, 66)
        Me.btnProducto.TabIndex = 25
        Me.btnProducto.Text = "Producto"
        Me.btnProducto.UseVisualStyleBackColor = True
        '
        'btnProceso1
        '
        Me.btnProceso1.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProceso1.Location = New System.Drawing.Point(415, 211)
        Me.btnProceso1.Name = "btnProceso1"
        Me.btnProceso1.Size = New System.Drawing.Size(159, 66)
        Me.btnProceso1.TabIndex = 27
        Me.btnProceso1.Text = "Proceso"
        Me.btnProceso1.UseVisualStyleBackColor = True
        '
        'lblCi
        '
        Me.lblCi.AutoSize = True
        Me.lblCi.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCi.Location = New System.Drawing.Point(289, 28)
        Me.lblCi.Name = "lblCi"
        Me.lblCi.Size = New System.Drawing.Size(48, 21)
        Me.lblCi.TabIndex = 28
        Me.lblCi.Text = "Altas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(277, 172)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(74, 21)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Ingresos"
        '
        'frmIngresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblCi)
        Me.Controls.Add(Me.btnProceso1)
        Me.Controls.Add(Me.btnTrasiego)
        Me.Controls.Add(Me.btnProducto)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnProceso)
        Me.Controls.Add(Me.btnUva)
        Me.Controls.Add(Me.btnRecipiente)
        Me.Name = "frmIngresos"
        Me.Text = "Ingresos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnRecipiente As Button
    Friend WithEvents btnUva As Button
    Friend WithEvents btnProceso As Button
    Friend WithEvents btnVolver As Button
    Friend WithEvents btnTrasiego As Button
    Friend WithEvents btnProducto As Button
    Friend WithEvents btnProceso1 As Button
    Friend WithEvents lblCi As Label
    Friend WithEvents Label1 As Label
End Class
