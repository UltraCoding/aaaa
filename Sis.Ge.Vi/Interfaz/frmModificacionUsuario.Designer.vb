﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificacionUsuario
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbRol = New System.Windows.Forms.ComboBox()
        Me.lblRol = New System.Windows.Forms.Label()
        Me.dtpFechaDeNacimiento = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaDeNacimiento = New System.Windows.Forms.Label()
        Me.btnAddTel = New System.Windows.Forms.Button()
        Me.cbTel = New System.Windows.Forms.ComboBox()
        Me.txtTel = New System.Windows.Forms.TextBox()
        Me.txtNPuerta = New System.Windows.Forms.TextBox()
        Me.txtEsquina = New System.Windows.Forms.TextBox()
        Me.txtCalle = New System.Windows.Forms.TextBox()
        Me.txtCI = New System.Windows.Forms.TextBox()
        Me.txtApellido = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.lblNPuerta = New System.Windows.Forms.Label()
        Me.lblEsquina = New System.Windows.Forms.Label()
        Me.lblCalle = New System.Windows.Forms.Label()
        Me.lblTel = New System.Windows.Forms.Label()
        Me.lblCi = New System.Windows.Forms.Label()
        Me.lblApellido = New System.Windows.Forms.Label()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.btnIngresar1 = New System.Windows.Forms.Button()
        Me.btnRemoveTel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbRol
        '
        Me.cbRol.FormattingEnabled = True
        Me.cbRol.Location = New System.Drawing.Point(321, 251)
        Me.cbRol.Name = "cbRol"
        Me.cbRol.Size = New System.Drawing.Size(100, 21)
        Me.cbRol.TabIndex = 49
        '
        'lblRol
        '
        Me.lblRol.AutoSize = True
        Me.lblRol.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.lblRol.Location = New System.Drawing.Point(79, 251)
        Me.lblRol.Name = "lblRol"
        Me.lblRol.Size = New System.Drawing.Size(30, 19)
        Me.lblRol.TabIndex = 48
        Me.lblRol.Text = "Rol"
        '
        'dtpFechaDeNacimiento
        '
        Me.dtpFechaDeNacimiento.Location = New System.Drawing.Point(321, 225)
        Me.dtpFechaDeNacimiento.Name = "dtpFechaDeNacimiento"
        Me.dtpFechaDeNacimiento.Size = New System.Drawing.Size(209, 20)
        Me.dtpFechaDeNacimiento.TabIndex = 47
        '
        'lblFechaDeNacimiento
        '
        Me.lblFechaDeNacimiento.AutoSize = True
        Me.lblFechaDeNacimiento.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.lblFechaDeNacimiento.Location = New System.Drawing.Point(79, 225)
        Me.lblFechaDeNacimiento.Name = "lblFechaDeNacimiento"
        Me.lblFechaDeNacimiento.Size = New System.Drawing.Size(133, 19)
        Me.lblFechaDeNacimiento.TabIndex = 46
        Me.lblFechaDeNacimiento.Text = "Fecha de nacimiento"
        '
        'btnAddTel
        '
        Me.btnAddTel.Location = New System.Drawing.Point(295, 121)
        Me.btnAddTel.Name = "btnAddTel"
        Me.btnAddTel.Size = New System.Drawing.Size(20, 20)
        Me.btnAddTel.TabIndex = 45
        Me.btnAddTel.Text = "+"
        Me.btnAddTel.UseVisualStyleBackColor = True
        '
        'cbTel
        '
        Me.cbTel.FormattingEnabled = True
        Me.cbTel.Location = New System.Drawing.Point(439, 120)
        Me.cbTel.Name = "cbTel"
        Me.cbTel.Size = New System.Drawing.Size(100, 21)
        Me.cbTel.TabIndex = 44
        '
        'txtTel
        '
        Me.txtTel.Location = New System.Drawing.Point(321, 121)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.Size = New System.Drawing.Size(100, 20)
        Me.txtTel.TabIndex = 43
        '
        'txtNPuerta
        '
        Me.txtNPuerta.Location = New System.Drawing.Point(321, 199)
        Me.txtNPuerta.Name = "txtNPuerta"
        Me.txtNPuerta.Size = New System.Drawing.Size(100, 20)
        Me.txtNPuerta.TabIndex = 42
        '
        'txtEsquina
        '
        Me.txtEsquina.Location = New System.Drawing.Point(321, 173)
        Me.txtEsquina.Name = "txtEsquina"
        Me.txtEsquina.Size = New System.Drawing.Size(100, 20)
        Me.txtEsquina.TabIndex = 41
        '
        'txtCalle
        '
        Me.txtCalle.Location = New System.Drawing.Point(321, 147)
        Me.txtCalle.Name = "txtCalle"
        Me.txtCalle.Size = New System.Drawing.Size(100, 20)
        Me.txtCalle.TabIndex = 40
        '
        'txtCI
        '
        Me.txtCI.Location = New System.Drawing.Point(321, 12)
        Me.txtCI.Name = "txtCI"
        Me.txtCI.Size = New System.Drawing.Size(100, 20)
        Me.txtCI.TabIndex = 39
        '
        'txtApellido
        '
        Me.txtApellido.Location = New System.Drawing.Point(321, 95)
        Me.txtApellido.Name = "txtApellido"
        Me.txtApellido.Size = New System.Drawing.Size(100, 20)
        Me.txtApellido.TabIndex = 38
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(321, 69)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(100, 20)
        Me.txtNombre.TabIndex = 37
        '
        'lblNPuerta
        '
        Me.lblNPuerta.AutoSize = True
        Me.lblNPuerta.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNPuerta.Location = New System.Drawing.Point(78, 201)
        Me.lblNPuerta.Name = "lblNPuerta"
        Me.lblNPuerta.Size = New System.Drawing.Size(89, 19)
        Me.lblNPuerta.TabIndex = 36
        Me.lblNPuerta.Text = "N° de Puerta"
        '
        'lblEsquina
        '
        Me.lblEsquina.AutoSize = True
        Me.lblEsquina.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEsquina.Location = New System.Drawing.Point(78, 174)
        Me.lblEsquina.Name = "lblEsquina"
        Me.lblEsquina.Size = New System.Drawing.Size(56, 19)
        Me.lblEsquina.TabIndex = 35
        Me.lblEsquina.Text = "Esquina"
        '
        'lblCalle
        '
        Me.lblCalle.AutoSize = True
        Me.lblCalle.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalle.Location = New System.Drawing.Point(78, 147)
        Me.lblCalle.Name = "lblCalle"
        Me.lblCalle.Size = New System.Drawing.Size(40, 19)
        Me.lblCalle.TabIndex = 34
        Me.lblCalle.Text = "Calle"
        '
        'lblTel
        '
        Me.lblTel.AutoSize = True
        Me.lblTel.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTel.Location = New System.Drawing.Point(78, 121)
        Me.lblTel.Name = "lblTel"
        Me.lblTel.Size = New System.Drawing.Size(77, 19)
        Me.lblTel.TabIndex = 33
        Me.lblTel.Text = "Teléfono(s)"
        '
        'lblCi
        '
        Me.lblCi.AutoSize = True
        Me.lblCi.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCi.Location = New System.Drawing.Point(78, 11)
        Me.lblCi.Name = "lblCi"
        Me.lblCi.Size = New System.Drawing.Size(25, 19)
        Me.lblCi.TabIndex = 32
        Me.lblCi.Text = "CI"
        '
        'lblApellido
        '
        Me.lblApellido.AutoSize = True
        Me.lblApellido.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApellido.Location = New System.Drawing.Point(78, 94)
        Me.lblApellido.Name = "lblApellido"
        Me.lblApellido.Size = New System.Drawing.Size(60, 19)
        Me.lblApellido.TabIndex = 31
        Me.lblApellido.Text = "Apellido"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.Location = New System.Drawing.Point(78, 70)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(60, 19)
        Me.lblNombre.TabIndex = 30
        Me.lblNombre.Text = "Nombre"
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(82, 316)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 51
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(472, 316)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 50
        Me.btnIngresar.Text = "Aceptar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'btnIngresar1
        '
        Me.btnIngresar1.Location = New System.Drawing.Point(455, 12)
        Me.btnIngresar1.Name = "btnIngresar1"
        Me.btnIngresar1.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar1.TabIndex = 52
        Me.btnIngresar1.Text = "Ingresar"
        Me.btnIngresar1.UseVisualStyleBackColor = True
        '
        'btnRemoveTel
        '
        Me.btnRemoveTel.Location = New System.Drawing.Point(545, 121)
        Me.btnRemoveTel.Name = "btnRemoveTel"
        Me.btnRemoveTel.Size = New System.Drawing.Size(20, 20)
        Me.btnRemoveTel.TabIndex = 53
        Me.btnRemoveTel.Text = "-"
        Me.btnRemoveTel.UseVisualStyleBackColor = True
        '
        'frmModificacionUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnRemoveTel)
        Me.Controls.Add(Me.btnIngresar1)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.cbRol)
        Me.Controls.Add(Me.lblRol)
        Me.Controls.Add(Me.dtpFechaDeNacimiento)
        Me.Controls.Add(Me.lblFechaDeNacimiento)
        Me.Controls.Add(Me.btnAddTel)
        Me.Controls.Add(Me.cbTel)
        Me.Controls.Add(Me.txtTel)
        Me.Controls.Add(Me.txtNPuerta)
        Me.Controls.Add(Me.txtEsquina)
        Me.Controls.Add(Me.txtCalle)
        Me.Controls.Add(Me.txtCI)
        Me.Controls.Add(Me.txtApellido)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblNPuerta)
        Me.Controls.Add(Me.lblEsquina)
        Me.Controls.Add(Me.lblCalle)
        Me.Controls.Add(Me.lblTel)
        Me.Controls.Add(Me.lblCi)
        Me.Controls.Add(Me.lblApellido)
        Me.Controls.Add(Me.lblNombre)
        Me.Name = "frmModificacionUsuario"
        Me.Text = "Modificación de usuario"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbRol As ComboBox
    Friend WithEvents lblRol As Label
    Friend WithEvents dtpFechaDeNacimiento As DateTimePicker
    Friend WithEvents lblFechaDeNacimiento As Label
    Friend WithEvents btnAddTel As Button
    Friend WithEvents cbTel As ComboBox
    Friend WithEvents txtTel As TextBox
    Friend WithEvents txtNPuerta As TextBox
    Friend WithEvents txtEsquina As TextBox
    Friend WithEvents txtCalle As TextBox
    Friend WithEvents txtCI As TextBox
    Friend WithEvents txtApellido As TextBox
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents lblNPuerta As Label
    Friend WithEvents lblEsquina As Label
    Friend WithEvents lblCalle As Label
    Friend WithEvents lblTel As Label
    Friend WithEvents lblCi As Label
    Friend WithEvents lblApellido As Label
    Friend WithEvents lblNombre As Label
    Friend WithEvents btnVolver As Button
    Friend WithEvents btnIngresar As Button
    Friend WithEvents btnIngresar1 As Button
    Friend WithEvents btnRemoveTel As Button
End Class
