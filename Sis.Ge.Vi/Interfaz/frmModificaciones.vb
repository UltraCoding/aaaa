﻿Public Class frmModificaciones
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmMenuAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnProducto_Click(sender As Object, e As EventArgs) Handles btnProducto.Click
        frmModificacionProducto.Show()
        Dispose()
    End Sub

    Private Sub btnRecipiente_Click(sender As Object, e As EventArgs) Handles btnRecipiente.Click
        frmModificacionRecipiente.Show()
        Dispose()
    End Sub

    Private Sub btnProceso_Click(sender As Object, e As EventArgs) Handles btnProceso.Click
        frmModificacionProceso.Show()
        Dispose()
    End Sub
End Class