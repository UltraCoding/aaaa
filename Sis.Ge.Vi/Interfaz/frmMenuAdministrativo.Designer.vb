﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenuAdministrativo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnConsultas = New System.Windows.Forms.Button()
        Me.btnIngresos = New System.Windows.Forms.Button()
        Me.btnModificaciones = New System.Windows.Forms.Button()
        Me.btnBorrados = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnConsultas
        '
        Me.btnConsultas.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultas.Location = New System.Drawing.Point(49, 95)
        Me.btnConsultas.Name = "btnConsultas"
        Me.btnConsultas.Size = New System.Drawing.Size(231, 66)
        Me.btnConsultas.TabIndex = 0
        Me.btnConsultas.Text = "Consultas"
        Me.btnConsultas.UseVisualStyleBackColor = True
        '
        'btnIngresos
        '
        Me.btnIngresos.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIngresos.Location = New System.Drawing.Point(326, 95)
        Me.btnIngresos.Name = "btnIngresos"
        Me.btnIngresos.Size = New System.Drawing.Size(231, 66)
        Me.btnIngresos.TabIndex = 1
        Me.btnIngresos.Text = "Ingresos"
        Me.btnIngresos.UseVisualStyleBackColor = True
        '
        'btnModificaciones
        '
        Me.btnModificaciones.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModificaciones.Location = New System.Drawing.Point(49, 197)
        Me.btnModificaciones.Name = "btnModificaciones"
        Me.btnModificaciones.Size = New System.Drawing.Size(231, 66)
        Me.btnModificaciones.TabIndex = 2
        Me.btnModificaciones.Text = "Modificaciones"
        Me.btnModificaciones.UseVisualStyleBackColor = True
        '
        'btnBorrados
        '
        Me.btnBorrados.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBorrados.Location = New System.Drawing.Point(326, 197)
        Me.btnBorrados.Name = "btnBorrados"
        Me.btnBorrados.Size = New System.Drawing.Size(231, 66)
        Me.btnBorrados.TabIndex = 3
        Me.btnBorrados.Text = "Borrados"
        Me.btnBorrados.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(32, 313)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 24
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'frmMenuAdministrativo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnBorrados)
        Me.Controls.Add(Me.btnModificaciones)
        Me.Controls.Add(Me.btnIngresos)
        Me.Controls.Add(Me.btnConsultas)
        Me.Name = "frmMenuAdministrativo"
        Me.Text = "Menú administrativo"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnConsultas As System.Windows.Forms.Button
    Friend WithEvents btnIngresos As System.Windows.Forms.Button
    Friend WithEvents btnModificaciones As System.Windows.Forms.Button
    Friend WithEvents btnBorrados As Button
    Friend WithEvents btnVolver As Button
End Class
