﻿Public Class Usuario

    Private _rol As Integer
    Private _ci As String
    Private _password As String
    Public Enum EnumRol
        Gerente
        Administrativo
        Cliente
        Asesor
        Conductor
    End Enum

    Public Property rol As Integer
        Get
            Return _rol
        End Get
        Set(value As Integer)
            _rol = value
        End Set
    End Property

    Public Property ci As String
        Get
            Return _ci
        End Get
        Set(value As String)
            _ci = value
        End Set
    End Property

    Public Property password As String
        Get
            Return _password
        End Get
        Set(value As String)
            _password = value
        End Set
    End Property
End Class