﻿Imports Dominio
Public Class Queries
    Inherits DBConnection
    Public Function getUserRole(user As Usuario) As Integer
        Dim sql As String
        sql = "SELECT rol FROM Usuario WHERE CI = " & user.ci
        Try
            rs = cn.Execute(sql)
        Catch ex As Exception

        End Try

        Return assignRole()
    End Function

    Private Function assignRole() As Integer
        Dim Role As Integer
        Select Case rs(0).Value
            Case "Gerente"
                Role = Usuario.EnumRol.Gerente
            Case "Administrativo"
                Role = Usuario.EnumRol.Administrativo
            Case "Cliente"
                Role = Usuario.EnumRol.Cliente
            Case "Asesor"
                Role = Usuario.EnumRol.Asesor
            Case "Conductor"
                Role = Usuario.EnumRol.Conductor
        End Select

        Return Role
    End Function
End Class
